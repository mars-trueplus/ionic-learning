import {Component} from '@angular/core';

import {NavController, NavParams} from 'ionic-angular';

import {ActionPigPage} from '../action-sheet/action-sheet';

// import {Platform} from '@ionic-angular';

@Component({
    templateUrl: 'examples.html'
})

export class ExamplePage{
    constructor(
        public navControl: NavController
    ){}

    openActionPigOhShit() {
        this.navControl.push(ActionPigPage);
    }
}