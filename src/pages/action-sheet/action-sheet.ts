import { Component } from '@angular/core';

import { Platform, ActionSheetController } from 'ionic-angular';

@Component({
    templateUrl: 'action-sheet.html'
})

export class ActionPigPage{
    constructor(
        public platform: Platform,
        public actionsheetCtrl: ActionSheetController
    ){}

    openSheet(){
        let actionSheet = this.actionsheetCtrl.create({
            title: 'Albums',
            cssClass: 'action-sheets-basic-page',
            buttons: [
                {
                    text: 'Delete',
                    role: 'destructive',
                    icon: !this.platform.is('ios') ? 'trash' : null,
                    handler: () => {
                        console.log('Delete clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    }
}